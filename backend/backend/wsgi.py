import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend.settings")

# expose WSGI callable as module-level variable named 'application'
# https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
application = get_wsgi_application()
