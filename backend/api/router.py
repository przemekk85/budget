from rest_framework import routers

from api import views


router = routers.DefaultRouter()
router.register(r"accounts", views.AccountViewset, basename="account")
router.register(r"tags", views.TagViewset, basename="tag")
router.register(r"transactions", views.TransactionViewset, basename="transaction")
