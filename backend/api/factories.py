from random import choice, randrange, shuffle

from django.db.models.signals import post_save
from factory import Faker, lazy_attribute, post_generation, SubFactory
from factory.django import DjangoModelFactory, mute_signals
from factory.fuzzy import FuzzyChoice, FuzzyInteger
import pendulum

from api.commons import TODAY
from api.enums import TransactionType
from api.models import Account, Limit, QuickTransaction, Tag, Transaction, User
from api.tests.helpers import get_balance


TRANSACTION_TYPES = [
    *1 * [TransactionType.CREDIT.value],
    *9 * [TransactionType.CHARGE.value],
]
shuffle(TRANSACTION_TYPES)


class UserFactory(DjangoModelFactory):
    """User factory."""

    email = Faker("email")
    password = ""

    class Meta:
        """Meta."""

        model = User

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        """Create instance."""
        manager = cls._get_manager(model_class)
        return manager.create_user(*args, **kwargs)


class AccountFactory(DjangoModelFactory):
    """Account factory."""

    owner = SubFactory(UserFactory)

    class Meta:
        """Meta."""

        model = Account

    @lazy_attribute
    def text(self):
        """Generate 'text' field."""
        return f"{self.owner.email}'s account #{self.owner.own_accounts.count() + 1}"

    @post_generation
    def authorized(self, create, extracted, **kwargs):
        """Generate 'authorized' field."""
        if not create:
            return
        if extracted:
            self.authorized.set(extracted)
            return
        self.authorized.set(UserFactory.create_batch(kwargs.get("count", 0)))

    @post_generation
    def tags(self, create, extracted, **kwargs):
        """Generate 'tags' field."""
        if not create:
            return
        if extracted:
            self.tags.set(extracted)
            return
        TagFactory.create_batch(kwargs.get("count", 0), account=self, owner=self.owner)

    @post_generation
    @mute_signals(post_save)
    def transactions(self, create, extracted, **kwargs):
        """Generate 'transactions' field."""
        if not create or extracted:
            return
        days = kwargs.get("days", 0)
        if days:
            initial_dt = TODAY.subtract(days=days).subtract(days=kwargs.get("shift", 0))
            current_dt = initial_dt
            current_month = current_dt.month
            current_year = current_dt.year
            TransactionFactory(
                value=randrange(3000, 3500, 100),
                created=current_dt,
                type=TransactionType.BALANCE.value,
                account=self,
            )
            for i in range(1, days + 1):
                if not Limit.objects.filter(
                    tag__account=self, month=current_month, year=current_year
                ).exists():
                    for tag in self.tags.all():
                        LimitFactory(
                            total=randrange(500, 3000, 500),
                            month=current_month,
                            year=current_year,
                            tag=tag,
                        )
                TransactionFactory.create_batch(
                    3,
                    created=current_dt,
                    type=TransactionType.CHARGE.value,
                    account=self,
                )
                current_dt = current_dt.add(days=1)
                if current_month != current_dt.month:
                    TransactionFactory(
                        value=get_balance(
                            self.transactions, dt=(current_dt.subtract(days=1))
                        ),
                        created=current_dt,
                        type=TransactionType.SYSTEM_BALANCE.value,
                        account=self,
                    )
                    TransactionFactory(
                        value=4000,
                        created=current_dt,
                        type=TransactionType.CREDIT.value,
                        account=self,
                    )
                    current_month = current_dt.month
                    current_year = current_dt.year


class LimitFactory(DjangoModelFactory):
    """Limit factory."""

    total = FuzzyInteger(1000, 2000, 100)
    month = TODAY.month
    year = TODAY.year

    class Meta:
        """Meta."""

        model = Limit


class QuickTransactionFactory(DjangoModelFactory):
    """Quick transaction factory."""

    class Meta:
        """Meta."""

        model = QuickTransaction


class TagFactory(DjangoModelFactory):
    """Tag factory."""

    class Meta:
        """Meta."""

        model = Tag

    @lazy_attribute
    def text(self):
        """Generate 'text' field."""
        return f"{self.owner}'s tag #{self.owner.own_tags.count() + 1}"

    @lazy_attribute
    def owner(self):
        """Generate 'owner' field."""
        return self.account.owner


class TransactionFactory(DjangoModelFactory):
    """Transaction factory."""

    type = FuzzyChoice(TRANSACTION_TYPES)

    class Meta:
        """Meta."""

        model = Transaction

    @lazy_attribute
    def value(self):
        """Generate 'value' field."""
        return {
            TransactionType.CHARGE.value: randrange(10, 100, 10),
            TransactionType.CREDIT.value: randrange(100, 1000, 100),
        }.get(self.type)

    @lazy_attribute
    def created(self):
        """Generate 'created' field"""
        created = self.account.transactions.last().created
        return (
            pendulum.date(created.year, created.month, created.day).add(days=1) or TODAY
        )

    @post_generation
    def tags(self, create, extracted, **kwargs):
        """Generate 'tags' field."""
        if not create:
            return
        if extracted:
            self.tags.set(extracted)
            return
        if self.account.tags.exists() and self.type not in [
            TransactionType.BALANCE.value,
            TransactionType.SYSTEM_BALANCE.value,
        ]:
            self.tags.add(choice(self.account.tags.all()))

    @lazy_attribute
    def owner(self):
        """Generate 'owner' field."""
        return choice([self.account.owner, *self.account.authorized.all()])
