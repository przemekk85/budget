from rest_framework.permissions import BasePermission

from api.models import Account


class AccountPermission(BasePermission):
    """Account permission."""

    def has_object_permission(self, request, view, obj):
        """Object permission."""
        return obj.has_object_read_permission(request)

    def has_permission(self, request, view):
        """Permission."""
        return Account.has_read_permission(request)
