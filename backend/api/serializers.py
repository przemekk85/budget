from rest_framework import serializers

from api.commons import TODAY
from api.enums import TransactionType
from api.validators import TagsApplicable, TagsInAccountTags
from api.models import Account, Limit, Tag, Transaction, User


class UserSerializer(serializers.ModelSerializer):
    """User serializer."""

    id = serializers.IntegerField(read_only=True)

    class Meta:
        """Meta."""

        fields = ("id", "email")
        model = User


class AccountDetailsSerializer(serializers.ModelSerializer):
    """Account serializer - details."""

    id = serializers.IntegerField(read_only=True)
    balance = serializers.DecimalField(decimal_places=2, max_digits=10, read_only=True)
    authorized = serializers.SlugRelatedField(
        many=True, queryset=User.objects.all(), required=False, slug_field="email"
    )
    owner = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        """Meta."""

        depth = 1
        fields = ("id", "text", "balance", "authorized", "owner")
        model = Account

    def update(self, instance, validated_data):
        """Update instance."""
        authorized = validated_data.pop("authorized", [])
        instance.text = validated_data.get("text", instance.text)
        if authorized:
            instance.authorized.set(authorized)
        instance.save()
        return instance


class DateslotsSerializer(serializers.Serializer):
    """Dateslots serializer."""

    month = serializers.IntegerField(max_value=12, min_value=1, source="created__month")
    year = serializers.IntegerField(
        max_value=TODAY.year, min_value=2000, source="created__year"
    )


class LimitSerializer(serializers.ModelSerializer):
    """Limit serializer."""

    id = serializers.IntegerField(read_only=True)

    class Meta:
        """Meta."""

        fields = ("id", "total", "tag")
        model = Limit


class TagListSerializer(serializers.ModelSerializer):
    """Tag serializer - list."""

    id = serializers.IntegerField(required=False)
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        """Meta."""

        fields = ("id", "is_active", "text")
        model = Tag


class TransactionSerializer(serializers.ModelSerializer):
    """Transaction serializer."""

    id = serializers.IntegerField(read_only=True)
    created = serializers.DateField(required=False)
    type = serializers.ChoiceField(choices=TransactionType.choices())
    account = serializers.PrimaryKeyRelatedField(queryset=Account.objects.all())
    owner = serializers.SlugRelatedField(read_only=True, slug_field="email")
    tags = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Tag.objects.filter(is_active=True), required=False,
    )

    class Meta:
        """Meta."""

        fields = ("id", "value", "created", "type", "account", "tags", "owner")
        model = Transaction
        validators = [
            TagsInAccountTags(),
            TagsApplicable([TransactionType.BALANCE.value]),
        ]

    def create(self, validated_data):
        """Create instance."""
        tags = validated_data.pop("tags", [])
        owner = self.context["request"].user
        transaction_instance = Transaction.objects.create(
            **{**validated_data, "owner": owner}
        )
        transaction_instance.tags.set(tags)
        return transaction_instance

    def update(self, instance, validated_data):
        """Update instance."""
        update_fields = validated_data.keys()
        tags = validated_data.pop("tags", [])
        instance.value = validated_data.get("value", instance.value)
        instance.type = validated_data.get("type", instance.type)
        if tags:
            instance.tags.set(tags)
        instance.save(update_fields=update_fields)
        return instance


class AccountListSerializer(serializers.ModelSerializer):
    """Account serializer - list."""

    id = serializers.IntegerField(read_only=True)
    is_own = serializers.BooleanField(read_only=True)
    tags = serializers.SerializerMethodField()

    class Meta:
        """Meta."""

        fields = ("id", "text", "is_own", "tags")
        model = Account

    def get_tags(self, obj):
        """Get tags."""
        qs = obj.tags.filter(is_active=True)
        serializer = TagListSerializer(qs, many=True)
        return serializer.data


class TagDetailsSerializer(serializers.ModelSerializer):
    """Tag serializer - details."""

    id = serializers.IntegerField(read_only=True)
    limit_total = serializers.SlugRelatedField(
        read_only=True, slug_field="total", source="limit",
    )
    limit_used = serializers.DecimalField(
        decimal_places=2, max_digits=10, read_only=True
    )
    transactions_count = serializers.IntegerField(read_only=True)
    limits = LimitSerializer(many=True, required=False)
    account = serializers.PrimaryKeyRelatedField(queryset=Account.objects.all())
    owner = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        """Meta."""

        fields = (
            "id",
            "text",
            "is_active",
            "limit_total",
            "limit_used",
            "transactions_count",
            "limits",
            "account",
            "owner",
        )
        model = Tag
