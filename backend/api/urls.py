from django.urls import include, path

from api import views
from api.router import router

urlpatterns = [
    path("", include(router.urls)),
    path(
        "dateslots/", views.DateslotsFullListView.as_view(), name="dateslots-full-list",
    ),
    path(
        "dateslots/<int:pk>",
        views.DateslotsAccountListView.as_view(),
        name="dateslots-account-list",
    ),
    path(
        "quicktransaction/<uuid:uuid>",
        views.QuickTransactionView.as_view(),
        name="quicktransaction",
    ),
]
