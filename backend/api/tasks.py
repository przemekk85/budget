from __future__ import absolute_import, unicode_literals
from abc import ABC

from celery import shared_task, Task
from django.db.utils import IntegrityError
from django.utils.translation import ugettext_lazy as _

from api.commons import TODAY
from api.enums import TransactionType
from api.models import Account, Limit, Tag, Transaction


class RecalculateSystemBalances(Task, ABC):
    """Recalculate system balances task."""

    _initial_args = ()
    _validated_args = ()

    @staticmethod
    def _run(instance_system_balance_id):
        r = []
        while instance_system_balance_id:
            try:
                instance = Transaction.objects.with_system_balance_id().get(
                    id=instance_system_balance_id
                )
            except Transaction.DoesNotExist:
                pass
            else:
                instance_system_balance_id = instance.system_balance_id
                value = instance.value
                instance.value = instance.account.balance(dt=instance.dt)
                instance.save()
                r.append(
                    f"\nid={instance.id} value=({value} -> {round(instance.value, 2)})"
                )
        return f"Updated transactions:{''.join(r)}"

    def validate(self):
        """Validate args."""
        (instance_system_balance_id, instance_type,) = self._initial_args

        if (
            instance_type != TransactionType.SYSTEM_BALANCE.value
            and instance_system_balance_id
        ):
            self._validated_args = (instance_system_balance_id,)


class SetLimits(Task, ABC):
    """Set limits task."""

    _dt = TODAY.subtract(months=1)

    @staticmethod
    def get_tag_or_none(tag_id):
        """Get Tag instance."""
        try:
            return Tag.objects.get(id=tag_id)
        except Tag.DoesNotExist:
            return None

    @staticmethod
    def set_limit(total, tag_instance):
        """Set limit."""
        try:
            Limit.objects.create(total=total, tag=tag_instance)
        except IntegrityError:
            return False
        else:
            return True


@shared_task(base=RecalculateSystemBalances, bind=True)
def recalculate_system_balances(self, *args):
    """Run task."""
    self._initial_args = args
    self.validate()
    if self._validated_args:
        return self._run(*self._validated_args)


@shared_task
def calculate_system_balances():
    """Run task."""
    dt = TODAY.subtract(months=1)
    dt = dt.replace(day=dt.days_in_month)
    i = 0
    for i, account_instance in enumerate(Account.objects.all()):
        balance = account_instance.balance(dt=dt)
        Transaction.objects.create(
            value=balance,
            type=TransactionType.SYSTEM_BALANCE.value,
            account=account_instance,
            owner=account_instance.owner,
        )
    return f"System balances have been calculated for {i} accounts"


@shared_task(base=SetLimits, bind=True)
def set_limits(self):
    """Run task."""
    failed_tag_ids = []
    i = 0
    for total, tag_id in Limit.objects.filter(
        month=self._dt.month, year=self._dt.year
    ).values_list("total", "tag"):
        if self.set_limit(total, self.get_tag_or_none(tag_id)):
            i += 1
        else:
            failed_tag_ids.append(tag_id)
    return f"Limits have been set for {i} tags"
