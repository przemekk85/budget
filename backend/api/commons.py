from django.conf import settings
import pendulum


TODAY = pendulum.today(tz=settings.TIME_ZONE).date()
