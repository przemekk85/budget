# Generated by Django 3.0 on 2019-12-23 18:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_auto_20191221_2259'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='account',
            options={'verbose_name': 'Account', 'verbose_name_plural': 'Accounts'},
        ),
        migrations.AlterModelOptions(
            name='limit',
            options={'verbose_name': 'Limit', 'verbose_name_plural': 'Limits'},
        ),
        migrations.AlterModelOptions(
            name='quicktransaction',
            options={'verbose_name': 'Quick transaction', 'verbose_name_plural': 'Quick transactions'},
        ),
        migrations.AlterModelOptions(
            name='tag',
            options={'verbose_name': 'Tag', 'verbose_name_plural': 'Tags'},
        ),
        migrations.AlterModelOptions(
            name='transaction',
            options={'verbose_name': 'Transaction', 'verbose_name_plural': 'Transactions'},
        ),
    ]
