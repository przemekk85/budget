from django.apps import AppConfig


class ApiConfig(AppConfig):
    """App config."""

    name = "api"
    verbose_name = "Budget API"
