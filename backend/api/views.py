from django.contrib.auth import login, logout
from django.db.models import Case, BooleanField, Q, Value, When
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from dry_rest_permissions.generics import DRYPermissions
from drf_yasg import openapi
from drf_yasg.openapi import (
    Parameter as OpenAPIParameter,
    Response as OpenAPIResponse,
)
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, views, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.commons import TODAY
from api.decorators import fake_qs
from api.enums import TransactionType
from api.filters import TransactionFilterSet
from api.mixins import PerformCeleryTaskMixin
from api.models import Account, QuickTransaction, Tag, Transaction
from api.permissions import AccountPermission
from api.serializers import (
    AccountDetailsSerializer,
    AccountListSerializer,
    DateslotsSerializer,
    LimitSerializer,
    TagDetailsSerializer,
    TagListSerializer,
    TransactionSerializer,
)


dateslots_param_id = OpenAPIParameter(
    "id",
    openapi.IN_PATH,
    description="A unique integer value identifying account",
    type=openapi.TYPE_INTEGER,
)
dateslots_response = OpenAPIResponse("", DateslotsSerializer(many=True))
quicktransaction_param_uuid = OpenAPIParameter(
    "uuid",
    openapi.IN_PATH,
    description="Universally unique identifier for adding transactions",
    format="uuid",
    type=openapi.TYPE_STRING,
)
quicktransaction_get_response = OpenAPIResponse("", AccountListSerializer(many=True))
quicktransaction_post_response = OpenAPIResponse("", TransactionSerializer())


class AccountViewset(viewsets.ModelViewSet):
    """API endpoint for account.

    ---
    create:
        Create account.
    delete:
        Delete this account.
    list:
        List accounts.
    partial_update:
        Update this account.
    retrieve:
        Get this account details.
    update:
        Update this account.

    """

    queryset = Account.objects.all()
    permission_classes = (DRYPermissions,)
    serializer_class = AccountDetailsSerializer

    @fake_qs(Account.objects.none())
    def get_queryset(self):
        """Get queryset."""
        user = self.request.user
        return (
            Account.objects.filter(Q(owner=user) | Q(authorized=user))
            .annotate(
                is_own=Case(
                    When(owner=user, then=Value(True)),
                    default=Value(False),
                    output_field=BooleanField(),
                )
            )
            .order_by("-is_own", "text")
            if self.action == "list"
            else super().get_queryset()
        )

    def get_serializer_class(self):
        """Get serializer class."""
        return (
            AccountListSerializer
            if self.action == "list"
            else super().get_serializer_class()
        )


class DateslotsAccountListView(views.APIView):
    """API endpoint for dateslots - for account."""

    permission_classes = (AccountPermission,)

    def get_object(self, request, pk):
        """Get object."""
        obj = get_object_or_404(Account, id=pk)
        self.check_object_permissions(request, obj)
        return obj

    # None object is neccessary due to handle drf_yasg package
    def get_queryset(self, obj=None):
        """Get queryset."""
        return (
            obj.transactions.values("created__month", "created__year").distinct()
            if obj
            else []
        )

    @swagger_auto_schema(
        manual_parameters=[dateslots_param_id],
        operation_description="List dateslots for account.",
        operation_id="dateslots_account_list",
        responses={200: dateslots_response},
    )
    def get(self, request, pk):
        """GET method."""
        account = self.get_object(request, pk)
        qs = self.get_queryset(account)
        serializer = DateslotsSerializer(qs, many=True)
        return Response(serializer.data)


class DateslotsFullListView(views.APIView):
    """API endpoint for dateslots - for transactions."""

    permission_classes = (AccountPermission,)

    @fake_qs(Transaction.objects.none())
    def get_queryset(self):
        """Get queryset."""
        user = self.request.user
        return (
            Transaction.objects.filter(
                Q(owner=user) | Q(account__owner=user) | Q(account__authorized=user)
            )
            .values("created__year", "created__month")
            .distinct()
        )

    @swagger_auto_schema(
        operation_description="List dateslots.",
        operation_id="dateslots_full_list",
        responses={200: dateslots_response},
    )
    def get(self, request):
        """GET method."""
        qs = self.get_queryset()
        serializer = DateslotsSerializer(qs, many=True)
        return Response(serializer.data)


class QuickTransactionView(views.APIView):
    """API endpoint for quick transaction."""

    permission_classes = ()

    def get_object(self, uuid):
        """Get object."""
        obj = get_object_or_404(QuickTransaction, uuid=uuid)
        return obj.owner

    def get_queryset(self, obj):
        """Get queryset."""
        return Account.objects.filter(Q(owner=obj) | Q(authorized=obj))

    @swagger_auto_schema(
        operation_description="List accounts.",
        operation_id="quicktransaction_list",
        responses={200: quicktransaction_get_response},
    )
    def get(self, request, uuid):
        """GET method."""
        user = self.get_object(uuid)
        qs = self.get_queryset(user)
        serializer = AccountListSerializer(qs, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        manual_parameters=[quicktransaction_param_uuid],
        operation_description="Add transaction.",
        responses={201: quicktransaction_post_response},
    )
    def post(self, request, uuid):
        """POST method."""
        user = self.get_object(uuid)
        login(request, user)
        serializer = TransactionSerializer(
            context={"request": request}, data=request.data
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        logout(request)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class TagViewset(viewsets.ModelViewSet):
    """API endpoint for tag.

    ---
    create:
        Create tag.
    delete:
        Delete this tag.
    list:
        List tags.
    partial_update:
        Update this tag.
    retrieve:
        Get this tag details.
    update:
        Update this tag.

    """

    queryset = Tag.objects.all()
    permission_classes = (DRYPermissions,)
    serializer_class = TagDetailsSerializer

    @fake_qs(Tag.objects.none())
    def get_queryset(self):
        """Get queryset."""
        return {
            "list": Tag.objects.filter(owner=self.request.user)
            .with_annotations()
            .order_by("account", "-is_active", "-transactions_count", "-id")
            .distinct()
        }.get(self.action, super().get_queryset())

    def get_serializer_class(self):
        """Get serializer class."""
        return {
            "list": TagListSerializer,
            "set_limit": LimitSerializer,
            "toggle_is_active": TagDetailsSerializer,
        }.get(self.action, super().get_serializer_class())

    @swagger_auto_schema(
        operation_description="Toggle 'is_active' field of this tag.",
        responses={200: None},
    )
    @action(detail=True, methods=["post"])
    def toggle_is_active(self, request, *args, **kwargs):
        """Toggle 'is_active' field."""
        instance = self.get_object()
        serializer = self.get_serializer(
            instance=instance, data={"is_active": not instance.is_active}, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_description="Set limit for this tag.",
        responses={200: LimitSerializer(), 201: LimitSerializer()},
    )
    @action(detail=True, methods=["post"])
    def set_limit(self, request, *args, **kwargs):
        """Create or update limit"""
        instance = self.get_object()
        had_limit = instance.limits.filter(month=TODAY.month, year=TODAY.year).exists()
        serializer = self.get_serializer(
            instance=instance.limits.filter(month=TODAY.month, year=TODAY.year).first(),
            data={**request.data, "tag": instance.id},
            partial=True,
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            serializer.data,
            status.HTTP_200_OK if had_limit else status.HTTP_201_CREATED,
        )

    @swagger_auto_schema(
        operation_description="Delete limit for this tag.", responses={204: None},
    )
    @action(detail=True, methods=["post"])
    def delete_limit(self, request, *args, **kwargs):
        """Destroy limit."""
        instance = self.get_object()
        instance.limits.filter(month=TODAY.month, year=TODAY.year).delete()
        return Response({}, status=status.HTTP_204_NO_CONTENT)

    def destroy(self, request, *args, **kwargs):
        """Destroy instance."""
        instance = self.get_object()
        if not instance.is_active:
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(
                {"detail": _("Method 'DELETE' is not allowed for active tag.")},
                status=status.HTTP_405_METHOD_NOT_ALLOWED,
            )

    def retrieve(self, request, *args, **kwargs):
        """Retrieve instance."""
        instance = self.get_object()
        if not instance.is_active:
            return Response(
                {"detail": _("Method 'GET' is not allowed for inactive tag.")},
                status=status.HTTP_405_METHOD_NOT_ALLOWED,
            )
        else:
            serializer = TagDetailsSerializer(instance)
            return Response(serializer.data, status=status.HTTP_200_OK)


class TransactionViewset(viewsets.ModelViewSet, PerformCeleryTaskMixin):
    """API endpoint for transaction.

    ---
    create:
        Create transaction.
    delete:
        Delete this transaction.
    list:
        List transactions.
    partial_update:
        Update this transaction.
    retrieve:
        Get this transaction details.
    update:
        Update this transaction.

    """

    filterset_class = TransactionFilterSet
    queryset = Transaction.objects.exclude(
        type=TransactionType.SYSTEM_BALANCE.value
    ).with_system_balance_id()
    permission_classes = (DRYPermissions,)
    serializer_class = TransactionSerializer

    def create(self, request, *args, **kwargs):
        """Create instance."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        instance = (
            Transaction.objects.filter(id=serializer.instance.id)
            .with_system_balance_id()
            .first()
        )
        self.perform_task(
            "api.tasks.recalculate_system_balances",
            instance.system_balance_id,
            instance.type,
        )
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def destroy(self, request, *args, **kwargs):
        """Destroy instance."""
        instance = self.get_object()
        self.perform_task(
            "api.tasks.recalculate_system_balances",
            instance.system_balance_id,
            instance.type,
        )
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """Update instance."""
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        instance = (
            Transaction.objects.filter(id=serializer.instance.id)
            .with_system_balance_id()
            .first()
        )
        self.perform_task(
            "api.tasks.recalculate_system_balances",
            instance.system_balance_id,
            instance.type,
        )
        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)

    @fake_qs(Transaction.objects.none())
    def get_queryset(self):
        """Get queryset."""
        qs = super().get_queryset()
        if self.action == "list":
            user = self.request.user
            qs = qs.filter(
                Q(owner=user) | Q(account__owner=user) | Q(account__authorized=user)
            )
        return qs.order_by("-created", "-id")
