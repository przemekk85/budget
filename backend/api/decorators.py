from functools import wraps


def fake_qs(qs):
    """Provide compatibility with schema generator."""
    def decorate(f):
        @wraps(f)
        def decorated(obj):
            if getattr(obj, "swagger_fake_view", False):
                # queryset just for schema generation metadata
                return qs
            return f(obj)
        return decorated
    return decorate
