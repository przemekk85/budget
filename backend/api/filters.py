from django_filters import CharFilter, FilterSet, ModelChoiceFilter

from api.models import Account, Transaction


class TransactionFilterSet(FilterSet):
    """Transaction filter set."""

    aid = ModelChoiceFilter(field_name="account", queryset=Account.objects.all())
    month = CharFilter(field_name="created__month", lookup_expr="iexact")
    year = CharFilter(field_name="created__year", lookup_expr="iexact")

    class Meta:
        """Meta."""

        fields = ["aid", "month", "year"]
        model = Transaction
