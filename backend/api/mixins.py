from importlib import import_module

from backend import celery_app


class PerformCeleryTaskMixin(object):
    """Perform Celery task mixin."""

    _task = None
    i = celery_app.control.inspect()

    @classmethod
    def perform_task(cls, name, *args):
        """Perform task."""
        if not cls._task:
            path = name.split(".")
            module = import_module(".".join(path[:-1]))
            cls._task = getattr(module, path[-1])
        if cls.i.ping():
            cls._task.delay(*args)

    @classmethod
    def task(cls):
        return cls._task
