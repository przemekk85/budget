from decimal import Decimal
from uuid import uuid4

from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import (
    Case,
    Count,
    F,
    OuterRef,
    Q,
    Sum,
    Value,
    Subquery,
    When,
)
from django.db.models.functions import Coalesce, ExtractMonth
from django.utils.translation import ugettext as _
import pendulum

from api.commons import TODAY
from api.enums import TransactionType


class UserManager(BaseUserManager):
    """Model manager for User model."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        """Create user."""
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create superuser."""
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    """Custom user model."""

    email = models.EmailField(_("email address"), unique=True)
    username = None

    objects = UserManager()

    REQUIRED_FIELDS = []
    USERNAME_FIELD = "email"

    def __repr__(self):
        """Representation."""
        return f"<{type(self).__name__}: id={self.id} email={self.email}>"

    def __str__(self):
        """Representation."""
        return self.email


class OwnedModel(models.Model):
    """Owned model abstract."""

    # https://docs.djangoproject.com/en/2.2/topics/db/models/#abstract-related-name
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="own_%(class)ss"
    )

    class Meta:
        """Meta."""

        abstract = True

    def __repr__(self):
        """Representation."""
        return f"<{type(self).__name__}: id={self.id} owner={self.owner}>"

    def __str__(self):
        """Representation."""
        return f"{self.owner.email}'s {type(self).__name__.lower()} ({self.id})"

    @staticmethod
    def has_read_permission(request):
        """Model read permission."""
        return request.user.is_authenticated

    @staticmethod
    def has_write_permission(request):
        """Model write permission."""
        return request.user.is_authenticated

    def has_object_write_permission(self, request):
        """Object write permission."""
        return request.user == self.owner

    def has_object_read_permission(self, request):
        """Object read permission."""
        return request.user == self.owner


class Account(OwnedModel):
    """Account model."""

    text = models.CharField(max_length=100)
    # relationships
    authorized = models.ManyToManyField(User, related_name="authorized_accounts")

    class Meta:
        """Meta."""

        verbose_name = _("Account")
        verbose_name_plural = _("Accounts")

    # TODO: move to queryset annotations
    def balance(self, dt=TODAY):
        """Get balance."""
        value = 0.00
        qs = self.transactions.filter(created__lte=dt)
        if qs.filter(
            type__in=[
                TransactionType.BALANCE.value,
                TransactionType.SYSTEM_BALANCE.value,
            ],
        ).exists():
            transaction_id, value, created, transaction_type = (
                qs.values_list("id", "value", "created", "type")
                .filter(
                    type__in=[
                        TransactionType.BALANCE.value,
                        TransactionType.SYSTEM_BALANCE.value,
                    ]
                )
                .order_by("created", "id")
                .last()
            )
            if transaction_type == TransactionType.BALANCE.value:
                qs = qs.exclude(
                    Q(created__lt=created) | Q(id__lt=transaction_id, created=created)
                )
            else:
                qs = qs.filter(created__gte=created.replace(day=1))
        balance = qs.aggregate(
            balance=(
                Coalesce(
                    Sum("value", filter=Q(type=TransactionType.CREDIT.value)), Value(0)
                )
                - Coalesce(
                    Sum("value", filter=Q(type=TransactionType.CHARGE.value)), Value(0)
                )
            )
        )["balance"]
        return round(balance + Decimal(value), 2)

    def __repr__(self):
        """Representation."""
        return (
            f"<{type(self).__name__}:"
            + f" id={self.id}"
            + f" owner={self.owner}"
            + f" authorized={list(self.authorized.all())}"
            + f" transactions_count={self.transactions.count()}"
            + f" balance={self.balance()}>"
        )

    def __str__(self):
        """Representation."""
        return _(f"{self.text}, owner: {self.owner}")

    def has_object_read_permission(self, request):
        """Object retrieve permission."""
        return self.authorized.filter(
            id=request.user.id
        ).exists() or super().has_object_read_permission(request)


class QuickTransaction(OwnedModel):
    """Quick transaction model."""

    uuid = models.UUIDField(default=uuid4, unique=True)

    class Meta:
        """Meta."""

        verbose_name = _("Quick transaction")
        verbose_name_plural = _("Quick transactions")

    def __repr__(self):
        """Representation."""
        return (
            f"<{type(self).__name__}:"
            + f" id={self.id}"
            + f" owner={self.owner}"
            + f" uuid={self.uuid}>"
        )


class TagQuerySet(models.QuerySet):
    """Tag queryset."""

    def with_annotations(self, dateslot=(TODAY.month, TODAY.year)):
        """Annotate queryset."""
        month, year = dateslot

        return self.annotate(
            limit_total=Case(
                When(
                    limits__isnull=False,
                    then=Subquery(
                        Limit.objects.filter(
                            month=month, year=year, tag=OuterRef("pk")
                        ).values("total"),
                        output_field=models.DecimalField(
                            decimal_places=2, max_digits=10
                        ),
                    ),
                ),
            ),
            limit_used=Case(
                When(
                    limits__isnull=False,
                    then=Subquery(
                        Limit.objects.filter(month=month, year=year, tag=OuterRef("pk"))
                        .with_used()
                        .values("used"),
                        output_field=models.DecimalField(
                            decimal_places=2, max_digits=10
                        ),
                    ),
                ),
            ),
            transactions_count=Subquery(
                Tag.objects.filter(id=OuterRef("pk"))
                .annotate(
                    cnt=Count(
                        "transactions",
                        filter=Q(
                            transactions__created__month=month,
                            transactions__created__year=year,
                        ),
                    )
                )
                .order_by("cnt")
                .values("cnt")[:1],
                output_field=models.IntegerField(),
            ),
        )


class Tag(OwnedModel):
    """Tag model."""

    text = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    # relationships
    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="tags")

    objects = TagQuerySet.as_manager()

    class Meta:
        """Meta."""

        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")

    def __repr__(self):
        """Representation."""
        msg = "(not present, call queryset using 'with_annotations' method)"
        limit_total_mgs = msg if not hasattr(self, "limit_total") else self.limit_total
        limit_used_mgs = msg if not hasattr(self, "limit_used") else self.limit_used
        transactions_count_msg = (
            msg if not hasattr(self, "transactions_count") else self.transactions_count
        )
        return (
            f"<{type(self).__name__}:"
            + f" id={self.id}"
            + f" is_active={self.is_active}"
            + f" owner={self.owner}"
            + f" account={self.account.id}"
            + f" limit_total={limit_total_mgs}"
            + f" limit_used={limit_used_mgs}"
            + f" transactions_count={transactions_count_msg}>"
        )

    def __str__(self):
        """Representation."""
        return _(f"{self.text}, account: {self.account.text}, owner: {self.owner}")

    @staticmethod
    def has_write_permission(request):
        """Model write permission."""
        account_id = request.data.get("account")
        if account_id:
            account = (
                Account.objects.get(id=account_id)
                if Account.objects.filter(id=account_id).exists()
                else None
            )
            return account and account.owner == request.user
        return super(Tag, Tag).has_write_permission(request)


class LimitQuerySet(models.QuerySet):
    """Limit queryset."""

    def with_used(self):
        """Annotate queryset."""

        return self.annotate(
            used=Case(
                When(
                    total__isnull=False,
                    then=Coalesce(
                        Sum(
                            "tag__transactions__value",
                            filter=Q(
                                tag__transactions__created__month=F("month"),
                                tag__transactions__created__year=F("year"),
                                tag__transactions__type=TransactionType.CREDIT.value,
                            ),
                        ),
                        Value(0.0),
                    )
                    - Coalesce(
                        Sum(
                            "tag__transactions__value",
                            filter=Q(
                                tag__transactions__created__month=F("month"),
                                tag__transactions__created__year=F("year"),
                                tag__transactions__type=TransactionType.CHARGE.value,
                            ),
                        ),
                        Value(0.0),
                    ),
                ),
                output_field=models.DecimalField(decimal_places=2, max_digits=10),
            )
        )


class Limit(models.Model):
    """Limit model."""

    total = models.DecimalField(decimal_places=2, max_digits=10)
    month = models.IntegerField(
        default=TODAY.month, validators=[MinValueValidator(1), MaxValueValidator(12)],
    )
    year = models.IntegerField(
        default=TODAY.year,
        validators=[
            MinValueValidator(TODAY.year - 2),
            MaxValueValidator(TODAY.year + 2),
        ],
    )
    # relationships
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE, related_name="limits")

    objects = LimitQuerySet.as_manager()

    class Meta:
        """Meta."""

        unique_together = ("month", "year", "tag")
        verbose_name = _("Limit")
        verbose_name_plural = _("Limits")

    def __repr__(self):
        """Representation."""
        return (
            f"<{type(self).__name__}:"
            + f" id={self.id}"
            + f" total={self.total}"
            + f" used={getattr(self, 'used', None)}"
            + f" month={self.month}"
            + f" year={self.year}"
            + f" tag={self.tag.id}>"
        )

    def __str__(self):
        """Representation."""
        return _(f"{self.year}-{self.month:02d}, tag: {self.tag.text}")

    @staticmethod
    def has_read_permission(request):
        """Model read permission."""
        return request.user.is_authenticated

    @staticmethod
    def has_write_permission(request):
        """Model write permission."""
        return request.user.is_authenticated


class TransactionQuerySet(models.QuerySet):
    """Transaction queryset."""

    def with_system_balance_id(self):
        """Annotate queryset.

        system_balance_id: points SYSTEM_BALANCE transaction, in which present
            transaction is included.

        """

        subquery = (
            Transaction.objects.filter(
                Q(id__gt=OuterRef("pk"), created=OuterRef("created"))
                | Q(created__gt=OuterRef("created")),
                type__in=[
                    TransactionType.BALANCE.value,
                    TransactionType.SYSTEM_BALANCE.value,
                ],
                account=OuterRef("account"),
            )
            .annotate(
                target_id=Case(
                    When(type=TransactionType.BALANCE.value, then=None,),
                    When(type=TransactionType.SYSTEM_BALANCE.value, then=F("id"),),
                    output_field=models.IntegerField(),
                ),
            )
            .order_by("created", "id")[:1]
        )

        return self.annotate(
            system_balance_id=Case(
                When(
                    ~Q(
                        created__month=ExtractMonth(
                            Subquery(
                                subquery.values("created"),
                                output_field=models.DateField(),
                            )
                        )
                    ),
                    then=Subquery(
                        subquery.values("target_id"), output_field=models.IntegerField()
                    ),
                ),
            ),
        )


class Transaction(OwnedModel):
    """Transaction model."""

    value = models.DecimalField(decimal_places=2, max_digits=10)
    created = models.DateField(null=True)
    type = models.IntegerField(choices=TransactionType.choices())
    # relationships
    account = models.ForeignKey(
        Account, on_delete=models.CASCADE, related_name="transactions"
    )
    tags = models.ManyToManyField(Tag, related_name="transactions")

    objects = TransactionQuerySet.as_manager()

    class Meta:
        """Meta."""

        verbose_name = _("Transaction")
        verbose_name_plural = _("Transactions")

    def __repr__(self):
        """Representation."""
        tags = list(self.tags.values_list("id", flat=True))
        system_balance_id_mgs = (
            "(not present, call queryset using 'with_system_balance_id' method)"
            if not hasattr(self, "system_balance_id")
            else self.system_balance_id
        )
        return (
            f"<{type(self).__name__}:"
            + f" owner={self.owner}"
            + f" id={self.id}"
            + f" value=({self.mark}{round(self.value, 2)})"
            + f" created={self.created}"
            + f" account={self.account.id}"
            + f" tags={tags}"
            + f" system_balance_id={system_balance_id_mgs}>"
        )

    def __str__(self):
        """Representation."""
        return _(
            f"{self.value_with_mark}, account: {self.account.text}, owner: {self.owner}"
        )

    def save(self, *args, **kwargs):
        """Save instance."""
        if not self.created:
            self.created = TODAY
        return super(self.__class__, self).save(*args, **kwargs)

    @property
    def dt(self):
        """Return date of the end of previous month.

        Works only for SYSTEM_BALANCE transactions.

        """
        if self.type == TransactionType.SYSTEM_BALANCE.value:
            dt = pendulum.date(
                self.created.year, self.created.month, self.created.day,
            ).subtract(months=1)
            dt = dt.replace(day=dt.days_in_month)
            return dt
        return None

    @property
    def value_with_mark(self):
        """Return sign of the transaction."""
        mark = {
            TransactionType.CHARGE.value: "-",
            TransactionType.CREDIT.value: "",
            TransactionType.BALANCE.value: "b ",
            TransactionType.SYSTEM_BALANCE.value: "B ",
        }.get(self.type)
        return f"{mark}{self.value}"

    @property
    def value_with_sign(self):
        """Return transaction value with sign."""
        sign = {TransactionType.CHARGE.value: -1}.get(self.type, 1)
        return sign * self.value

    @staticmethod
    def has_read_permission(request):
        """Model read permission."""
        account_id = request.query_params.get("aid")
        if account_id:
            account = (
                Account.objects.get(id=account_id)
                if Account.objects.filter(id=account_id).exists()
                else None
            )
            return (
                account
                and account.has_object_read_permission(request)
                and super(Transaction, Transaction).has_read_permission(request)
            )
        return super(Transaction, Transaction).has_read_permission(request)

    @staticmethod
    def has_write_permission(request):
        """Model write permission."""
        account_id = request.data.get("account")
        if account_id:
            account = (
                Account.objects.get(id=account_id)
                if Account.objects.filter(id=account_id).exists()
                else None
            )
            return (
                account
                and account.has_object_read_permission(request)
                and super(Transaction, Transaction).has_write_permission(request)
            )
        return super(Transaction, Transaction).has_write_permission(request)

    def has_object_read_permission(self, request):
        """Object read permission."""
        return self.account.has_object_read_permission(
            request
        ) or super().has_object_read_permission(request)

    def has_object_write_permission(self, request):
        """Object write permission."""
        return (
            super().has_object_write_permission(request)
            or self.account.owner == request.user
        )
