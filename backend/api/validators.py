from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError

from api.enums import TransactionType


class TagsBase(object):
    """Base class for tags validator."""

    instance = None

    @property
    def _is_create(self):
        return not bool(self.instance)

    def set_context(self, serializer):
        """Set context.

        This hook is called by the serializer instance, prior to the validation call
        being made.

        """
        self.instance = getattr(serializer, "instance", None)


class TagsInAccountTags(TagsBase):
    """Check tags presence.

    Validator that checks if given tags are present in tags of account that transaction
    is assigned to.

    """

    message = _("Given tags belongs to a different account.")

    def __init__(self, message=None):
        """Class constructor."""
        self.message = message or self.message

    def __call__(self, attrs):
        """Call validator."""
        if self._is_create:
            allowed_tags = attrs["account"].tags.all()
            data_tags = attrs.get("tags", [])
        elif self._is_patch_account(attrs):
            allowed_tags = attrs["account"].tags.all()
            data_tags = attrs.get("tags", self.instance.tags.all())
        else:
            allowed_tags = self.instance.account.tags.all()
            data_tags = attrs.get("tags", [])
        if set(data_tags).difference(allowed_tags):
            raise ValidationError(self.message)

    @staticmethod
    def _is_patch_account(attrs):
        return bool(attrs.get("account"))


class TagsApplicable(TagsBase):
    """Check if tags are applicable.

    Validator that checks if tags can be applied for transaction of given type.

    """

    message = _("Tags are not applicable for transaction of type {transaction_type}.")

    def __init__(self, excluded_types, message=None):
        """Class constructor."""
        self.excluded_types = excluded_types
        self.message = message or self.message

    def __call__(self, attrs):
        """Call validator."""
        if self._is_create:
            transaction_type = attrs.get("type", -1)
            if transaction_type in self.excluded_types and attrs.get("tags", []):
                raise ValidationError(
                    self.message.format(
                        transaction_type=TransactionType(transaction_type).name
                    )
                )
        else:
            transaction_type = attrs.get("type", self.instance.type)
            if transaction_type in self.excluded_types and attrs.get(
                "tags", self.instance.tags.all()
            ):
                raise ValidationError(
                    self.message.format(
                        transaction_type=TransactionType(transaction_type).name
                    )
                )
