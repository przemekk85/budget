from enum import auto, Enum


class TransactionType(Enum):
    """Transaction type."""

    CREDIT = auto()
    CHARGE = auto()
    BALANCE = auto()
    SYSTEM_BALANCE = auto()

    @classmethod
    def choices(cls):
        """Choices."""
        return tuple(
            (item.value, item.name)
            for item in cls
            if not item.name.startswith("SYSTEM_")
        )
