from django.urls import reverse


from api.factories import UserFactory
from api.tests.test_base import TestBaseDefault


class TestAsAuthorized(TestBaseDefault):
    """Tests for 'accounts' endpoint - authorized."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].authorized.first()
        self.client.force_authenticate(user=self.test_user)

    def test_delete(self):
        """."""
        response = self.client.delete(
            reverse("account-detail", args=[self.test_account.id])
        )
        self.assertEquals(403, response.status_code)

    def test_list(self):
        """."""
        response = self.client.get(reverse("account-list"))
        self.assertEquals(200, response.status_code)
        self.assertEquals(
            self.test_user.authorized_accounts.count(), len(response.json())
        )
        self.assertEquals(
            self.test_account.tags.filter(is_active=True).count(),
            len(response.json()[0].get("tags", [])),
        )

    def test_patch(self):
        """."""
        payload = {"text": f"patched {self.test_account.text}"}
        response = self.client.patch(
            reverse("account-detail", args=[self.test_account.id]), payload
        )
        self.assertEquals(403, response.status_code)

    def test_retrieve(self):
        """."""
        response = self.client.get(
            reverse("account-detail", args=[self.test_account.id])
        )
        self.assertEquals(200, response.status_code)


class TestAsOwner(TestBaseDefault):
    """Tests for 'accounts' endpoint - owner."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].owner
        self.client.force_authenticate(user=self.test_user)

    def test_create(self):
        """."""
        payload = {
            "text": f"another {self.test_user}'s account",
        }
        response = self.client.post(reverse("account-list"), payload)
        self.assertEquals(201, response.status_code)
        id = response.json().get("id")
        response = self.client.get(reverse("account-detail", args=[id]))
        self.assertEquals(200, response.status_code)

    def test_delete(self):
        """."""
        response = self.client.delete(
            reverse("account-detail", args=[self.test_account.id])
        )
        self.assertEquals(204, response.status_code)

    def test_list(self):
        """."""
        response = self.client.get(reverse("account-list"))
        self.assertEquals(200, response.status_code)
        self.assertEquals(self.test_user.own_accounts.count(), len(response.json()))
        self.assertEquals(
            self.test_account.tags.filter(is_active=True).count(),
            len(response.json()[0].get("tags", [])),
        )

    def test_patch(self):
        """."""
        payload = {"text": f"patched {self.test_account.text}"}
        response = self.client.patch(
            reverse("account-detail", args=[self.test_account.id]), payload
        )
        self.assertEquals(200, response.status_code)

    def test_retrieve(self):
        """."""
        response = self.client.get(
            reverse("account-detail", args=[self.test_account.id])
        )
        self.assertEquals(200, response.status_code)


class TestAsUnauthorized(TestBaseDefault):
    """Tests for 'accounts' endpoint - unauthorized."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[1]
        self.test_user = self.accounts[0].authorized.first()
        self.client.force_authenticate(user=self.test_user)

    def test_retrieve(self):
        """."""
        response = self.client.get(
            reverse("account-detail", args=[self.test_account.id])
        )
        self.assertEquals(403, response.status_code)


class TestFaultyRequest(TestBaseDefault):
    """Tests for 'accounts' endpoint - faulty request."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].authorized.first()
        self.client.force_authenticate(user=self.test_user)

    def test_create_with_invalid_authorized_user(self):
        """."""
        payload = {
            "text": f"another {self.test_user}'s account",
            "authorized": [UserFactory.build().email],
        }
        response = self.client.post(reverse("account-list"), payload)
        self.assertEquals(400, response.status_code)

    def test_retrieve_non_existing(self):
        """."""
        response = self.client.get(reverse("account-detail", args=[999]))
        self.assertEquals(404, response.status_code)
