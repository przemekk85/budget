from django.db.models.signals import post_save
from factory.django import mute_signals
from rest_framework.test import APITestCase

from api.factories import (
    AccountFactory,
    QuickTransactionFactory,
    UserFactory,
)


class TestBaseDefault(APITestCase):
    """Default test base."""

    @classmethod
    @mute_signals(post_save)
    def setUpTestData(cls):
        """Setup."""
        owner = UserFactory()
        cls.accounts = [
            AccountFactory(
                owner=owner, authorized__count=1, tags__count=3, transactions__days=35
            ),
            AccountFactory(owner=owner, tags__count=3, transactions__days=35),
        ]
        cls.quicktransaction = QuickTransactionFactory(owner=owner)


class TestBaseLong(APITestCase):
    """Base for tests - long."""

    @classmethod
    @mute_signals(post_save)
    def setUpTestData(cls):
        """Setup."""
        owner = UserFactory()
        cls.accounts = [
            AccountFactory(
                owner=owner, authorized__count=1, tags__count=3, transactions__days=182
            ),
            AccountFactory(owner=owner, tags__count=3, transactions__days=182),
        ]
        cls.quicktransaction = QuickTransactionFactory(owner=owner)
