from django.urls import reverse

from api.tests.test_base import TestBaseDefault


class TestAsAccountAuthorized(TestBaseDefault):
    """Tests for 'tags' endpoint - authorized."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].authorized.first()
        self.client.force_authenticate(user=self.test_user)

    def test_create(self):
        """."""
        payload = {
            "text": f"new {self.test_user}'s tag",
            "account": self.test_account.id,
        }
        response = self.client.post(reverse("tag-list"), payload)
        self.assertEquals(403, response.status_code)

    def test_delete(self):
        """."""
        test_tag = self.test_account.tags.last()
        response = self.client.delete(reverse("tag-detail", args=[test_tag.id]))
        self.assertEquals(403, response.status_code)

    def test_list(self):
        """."""
        response = self.client.get(reverse("tag-list"))
        self.assertEquals(200, response.status_code)
        self.assertEquals(self.test_user.own_tags.count(), len(response.json()))

    def test_patch(self):
        """."""
        test_tag = self.test_account.tags.last()
        payload = {"text": f"patched {test_tag.text}"}
        response = self.client.patch(reverse("tag-detail", args=[test_tag.id]), payload)
        self.assertEquals(403, response.status_code)

    def test_retrieve(self):
        """."""
        test_tag = self.test_account.tags.last()
        response = self.client.get(reverse("tag-detail", args=[test_tag.id]))
        self.assertEquals(403, response.status_code)


class TestAsAccountOwner(TestBaseDefault):
    """Tests for 'tags' endpoint - owner."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].owner
        self.client.force_authenticate(user=self.test_user)

    def test_create(self):
        """."""
        payload = {
            "text": f"new {self.test_user}'s tag",
            "account": self.test_account.id,
        }
        response = self.client.post(reverse("tag-list"), payload)
        self.assertEquals(201, response.status_code)
        id = response.json().get("id")
        response = self.client.get(reverse("tag-detail", args=[id]))
        self.assertEquals(200, response.status_code)

    def test_delete_active(self):
        """."""
        test_tag = self.test_account.tags.first()
        response = self.client.delete(reverse("tag-detail", args=[test_tag.id]))
        self.assertEquals(405, response.status_code)

    def test_delete_inactive(self):
        """."""
        test_tag = self.test_account.tags.first()
        test_tag.is_active = False
        test_tag.save()
        response = self.client.delete(reverse("tag-detail", args=[test_tag.id]))
        self.assertEquals(204, response.status_code)

    def test_list(self):
        """."""
        test_tag = self.test_account.tags.first()
        test_tag.is_active = False
        test_tag.save()
        response = self.client.get(reverse("tag-list"))
        self.assertEquals(200, response.status_code)
        self.assertEquals(self.test_user.own_tags.count(), len(response.json()))

    def test_patch(self):
        """."""
        test_tag = self.test_account.tags.first()
        payload = {
            "text": f"patched {test_tag.text}",
        }
        response = self.client.patch(reverse("tag-detail", args=[test_tag.id]), payload)
        self.assertEquals(200, response.status_code)

    def test_patch_delete_limit_when_has_limit(self):
        """."""
        test_tag = self.test_account.tags.first()
        expected_limits_count = test_tag.limits.count() - 1
        response = self.client.post(reverse("tag-delete-limit", args=[test_tag.id]))
        self.assertEquals(204, response.status_code)
        self.assertEquals(expected_limits_count, test_tag.limits.count())

    def test_patch_delete_limit_when_has_no_limit(self):
        """."""
        test_tag = self.test_account.tags.first()
        test_tag.limits.all().delete()
        response = self.client.post(reverse("tag-delete-limit", args=[test_tag.id]))
        self.assertEquals(204, response.status_code)

    def test_patch_set_limit_when_has_limit(self):
        """."""
        test_tag = self.test_account.tags.first()
        payload = {
            "total": 9999,
        }
        response = self.client.post(
            reverse("tag-set-limit", args=[test_tag.id]), payload
        )
        self.assertEquals(200, response.status_code)

    def test_patch_set_limit_when_has_no_limit(self):
        """."""
        test_tag = self.test_account.tags.first()
        test_tag.limits.all().delete()
        payload = {
            "total": 9999,
        }
        response = self.client.post(
            reverse("tag-set-limit", args=[test_tag.id]), payload
        )
        self.assertEquals(201, response.status_code)

    def test_retrieve_when_is_active(self):
        """."""
        test_tag = self.test_account.tags.first()
        response = self.client.get(reverse("tag-detail", args=[test_tag.id]))
        self.assertEquals(200, response.status_code)

    def test_retrieve_when_not_is_active(self):
        """."""
        test_tag = self.test_account.tags.first()
        test_tag.is_active = False
        test_tag.save()
        response = self.client.get(reverse("tag-detail", args=[test_tag.id]))
        self.assertEquals(405, response.status_code)

    def test_toggle_when_is_active(self):
        """."""
        test_tag = self.test_account.tags.first()
        response = self.client.post(reverse("tag-toggle-is-active", args=[test_tag.id]))
        self.assertEquals(200, response.status_code)
        response = self.client.get(reverse("tag-detail", args=[test_tag.id]))
        self.assertEquals(405, response.status_code)


class TestAsUnauthorized(TestBaseDefault):
    """Tests for 'tags' endpoint - unauthorized."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[1]
        self.test_user = self.accounts[0].authorized.first()
        self.client.force_authenticate(user=self.test_user)

    def test_create(self):
        """."""
        payload = {
            "text": f"new {self.test_user}'s tag",
            "account": self.test_account.id,
        }
        response = self.client.post(reverse("tag-list"), payload)
        self.assertEquals(403, response.status_code)

    def test_delete(self):
        """."""
        test_tag = self.test_account.tags.first()
        response = self.client.delete(reverse("tag-detail", args=[test_tag.id]))
        self.assertEquals(403, response.status_code)

    def test_list(self):
        """."""
        response = self.client.get(reverse("tag-list"))
        self.assertEquals(200, response.status_code)
        self.assertEquals(self.test_user.own_tags.count(), len(response.json()))

    def test_patch(self):
        """."""
        test_tag = self.test_account.tags.first()
        payload = {"text": f"patched {test_tag.text}"}
        response = self.client.patch(reverse("tag-detail", args=[test_tag.id]), payload)
        self.assertEquals(403, response.status_code)

    def test_retrieve(self):
        """."""
        test_tag = self.test_account.tags.first()
        response = self.client.get(reverse("tag-detail", args=[test_tag.id]))
        self.assertEquals(403, response.status_code)
