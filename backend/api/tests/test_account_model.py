from dateutil.relativedelta import relativedelta

from django.db.models.signals import post_save
from factory.django import mute_signals
import pendulum

from api.enums import TransactionType
from api.factories import AccountFactory, TransactionFactory
from api.tests.helpers import get_balance
from api.tests.test_base import TestBaseDefault


class TestAccountBalanceMethod(TestBaseDefault):
    """Tests for 'Account' model - balance method."""

    @classmethod
    def setUpTestData(cls):
        """Setup."""
        cls.test_account = AccountFactory(
            tags__count=1, transactions__days=35, transactions__shift=7
        )
        cls.test_user = cls.test_account.owner
        latest_date = cls.test_account.transactions.values("created").last()["created"]
        cls.latest_date = pendulum.date(
            latest_date.year, latest_date.month, latest_date.day
        )

    def test_clean(self):
        """."""
        expected_balance = get_balance(self.test_account.transactions)
        self.assertEqual(expected_balance, self.test_account.balance())

    def test_clean_with_date(self):
        dt = self.latest_date - relativedelta(months=1)
        expected_balance = get_balance(self.test_account.transactions, dt=dt)
        self.assertEqual(expected_balance, self.test_account.balance(dt=dt))

    def test_balance_transaction_is_last(self):
        with mute_signals(post_save):
            TransactionFactory(
                value=9999,
                type=TransactionType.BALANCE.value,
                account=self.test_account,
            )
        self.assertEqual(9999, self.test_account.balance())

    def test_balance_transaction_is_in_the_middle(self):
        with mute_signals(post_save):
            TransactionFactory(
                value=9999,
                type=TransactionType.BALANCE.value,
                account=self.test_account,
            )
            for i in range(3):
                TransactionFactory.create_batch(
                    3, created=self.latest_date.add(days=i), account=self.test_account,
                )
        self.assertEqual(
            get_balance(self.test_account.transactions), self.test_account.balance()
        )

    def test_balance_transaction_is_added_later(self):
        if self.latest_date.day <= 4:
            created = self.latest_date.subtract(days=7)
            dt = self.latest_date.subtract(months=1).end_of("month")
        else:
            created = self.latest_date.subtract(days=3)
            dt = self.latest_date
        with mute_signals(post_save):
            TransactionFactory(
                value=9999,
                created=created,
                type=TransactionType.BALANCE.value,
                account=self.test_account,
            )
        self.assertEqual(
            get_balance(self.test_account.transactions, dt=dt),
            self.test_account.balance(dt),
        )
