from decimal import Decimal

from api.enums import TransactionType


def get_balance(qs, dt=None):
    if dt:
        qs = qs.filter(created__lte=dt)
    balance = Decimal(0.00)
    for t in qs.order_by("created", "id"):
        if t.type == TransactionType.BALANCE.value:
            balance = t.value
        elif t.type == TransactionType.CHARGE.value:
            balance -= t.value
        elif t.type == TransactionType.CREDIT.value:
            balance += t.value
    return round(balance, 2)


def get_dateslots(qs=None, response=None):
    if not (qs or response):
        return {}
    else:
        return (
            {(t.created.month, t.created.year) for t in qs}
            if qs
            else {(ds.get("month"), ds.get("year")) for ds in response.json()}
        )


def get_opposite_type(transaction_type):
    return {
        TransactionType.CHARGE.value: TransactionType.CREDIT.value,
        TransactionType.CREDIT.value: TransactionType.CHARGE.value,
    }.get(transaction_type, transaction_type)
