from random import choice

from django.urls import reverse

from api.enums import TransactionType
from api.tests.test_base import TestBaseDefault


class TestQuickTransaction(TestBaseDefault):
    """Tests for 'quicktransaction' endpoint."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].owner

    def test_get(self):
        """."""
        response = self.client.get(
            reverse("quicktransaction", args=[self.quicktransaction.uuid])
        )
        self.assertEquals(
            self.test_user.own_accounts.count()
            + self.test_user.authorized_accounts.count(),
            len(response.json()),
        )

    def test_post(self):
        """."""
        payload = {
            "value": 100,
            "type": choice(
                [TransactionType.CREDIT.value, TransactionType.CHARGE.value]
            ),
            "account": self.test_account.id,
            "tags": [choice(self.test_account.tags.values_list("id", flat=True))],
        }
        response = self.client.post(
            reverse("quicktransaction", args=[self.quicktransaction.uuid]), payload,
        )
        self.assertEquals(201, response.status_code)
