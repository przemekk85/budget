from random import choice

from django.urls import reverse
from rest_framework.test import APITestCase

from api.factories import UserFactory


class TestUserBase(APITestCase):
    """Base for tests of 'users' endpoint."""

    __test__ = False

    def setUp(self):
        """Setup."""
        self.users = UserFactory.create_batch(10)
        self.test_user = self.users[0]


class TestUser(TestUserBase):
    """Tests for 'users' endpoint."""

    def setUp(self):
        """Setup."""
        super().setUp()
        self.client.force_authenticate(user=self.test_user)

    def test_list(self):
        """."""
        response = self.client.get(reverse("user-list"))
        self.assertEquals(403, response.status_code)
        # user should not see another users
        self.assertAlmostEqual(1, len(response.json()))

    def test_retrieve_other(self):
        """."""
        response = self.client.get(
            reverse(
                "user-detail",
                args=[
                    choice(
                        [user.id for user in self.users if user.id != self.test_user.id]
                    )
                ],
            )
        )
        self.assertEquals(403, response.status_code)

    def test_retrieve_self(self):
        """."""
        response = self.client.get(reverse("user-detail", args=[self.test_user.id]))
        self.assertEquals(200, response.status_code)
