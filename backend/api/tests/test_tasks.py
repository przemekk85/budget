from datetime import timedelta
from random import randrange
from time import time

from celery.contrib.testing.worker import start_worker
from django.urls import reverse
from django_celery_results.models import TaskResult
from rest_framework.test import APISimpleTestCase
import pendulum

from api.enums import TransactionType
from api.factories import AccountFactory
from api.models import Transaction
from api.tests.helpers import (
    get_balance,
    get_opposite_type,
)
from backend import celery_app


class TestRecalculateSystemBalancesTask(APISimpleTestCase):
    """Tests for Celery tasks."""

    databases = ("default",)

    @staticmethod
    def wait_for_success(timeout):
        initial_time = time()
        while True:
            task_name, status = TaskResult.objects.values_list(
                "task_name", "status"
            ).first() or ("", "")
            if initial_time + timeout < time():
                return False
            elif (
                task_name == "api.tasks.recalculate_system_balances"
                and status == "SUCCESS"
            ):
                return True

    def setUp(self):
        """Setup."""
        self.client.force_authenticate(user=self.test_user)

    @classmethod
    def setUpTestData(cls):
        cls.test_account = AccountFactory(transactions__days=70)
        cls.test_user = cls.test_account.owner
        cls.qs = cls.test_account.transactions.exclude(
            type=TransactionType.SYSTEM_BALANCE.value
        )
        earliest_date = cls.test_account.transactions.values("created").first()[
            "created"
        ]
        cls.earliest_date = pendulum.date(
            earliest_date.year, earliest_date.month, earliest_date.day
        )

    @classmethod
    def setUpClass(cls):
        """Class setup."""
        super().setUpClass()
        celery_app.loader.import_module("celery.contrib.testing.tasks")
        cls.celery_worker = start_worker(celery_app)
        cls.celery_worker.__enter__()
        cls.setUpTestData()

    def tearDown(self):
        """Teardown."""
        TaskResult.objects.filter(
            task_name="api.tasks.recalculate_system_balances", status="SUCCESS"
        ).delete()

    @classmethod
    def tearDownClass(cls):
        """Class teardown."""
        cls.celery_worker.__exit__(None, None, None)
        super().tearDownClass()

    def test_step_1_add_balance_transaction(self):
        """."""
        payload = {
            "value": 9999,
            "created": (self.earliest_date + timedelta(7)).strftime("%Y-%m-%d"),
            "type": TransactionType.BALANCE.value,
            "account": self.test_account.id,
        }
        self.client.post(reverse("transaction-list"), payload)
        self.wait_for_success(30)
        self.assertEqual(get_balance(self.qs), self.test_account.balance())

    def test_step_2_modify_unsettled_transaction(self):
        """."""
        test_transaction = self.qs.first()
        payload = {
            "value": test_transaction.value + randrange(100, 200, 10),
        }
        self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.wait_for_success(30)
        self.assertEqual(get_balance(self.qs), self.test_account.balance())

    def test_step_3_delete_balance_transaction(self):
        """."""
        test_transaction = self.qs.filter(type=TransactionType.BALANCE.value).last()
        self.client.delete(reverse("transaction-detail", args=[test_transaction.id]))
        self.wait_for_success(30)
        self.assertEqual(get_balance(self.qs), self.test_account.balance())

    def test_step_4_modify_settled_transaction_type(self):
        """."""
        test_transaction = self.qs.filter(
            created__gt=self.earliest_date + timedelta(14)
        ).first()
        payload = {
            "type": get_opposite_type(test_transaction.type),
        }
        self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.wait_for_success(30)
        self.assertEqual(get_balance(self.qs), self.test_account.balance())

    def test_step_5_modify_settled_transaction_value(self):
        """."""
        test_transaction = self.qs.filter(
            created__gt=self.earliest_date + timedelta(21)
        ).first()
        payload = {
            "value": test_transaction.value + randrange(100, 200, 10),
        }
        self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.wait_for_success(30)
        self.assertEqual(get_balance(self.qs), self.test_account.balance())

    def test_step_6_modify_settled_transaction_when_missing_system_balance(self):
        """."""
        # delete single SYSTEM_BALANCE transaction
        Transaction.objects.filter(
            type=TransactionType.SYSTEM_BALANCE.value
        ).first().delete()
        test_transaction = self.qs.first()
        payload = {
            "type": get_opposite_type(test_transaction.type),
        }
        self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.wait_for_success(30)
        self.assertEqual(get_balance(self.qs), self.test_account.balance())
