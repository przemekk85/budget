from decimal import Decimal

from django.test import TestCase

from api.factories import AccountFactory
from api.models import Tag, Transaction


class TestLimitUsedMethod(TestCase):
    """Tests for 'Limit' model - used method."""

    @staticmethod
    def _calculate_annotations(qs):
        limits_used = {}
        transactions_count = {}
        for t in qs.exclude(tags__isnull=True):
            ds = (t.created.month, t.created.year)
            if ds in limits_used:
                limits_used[ds] += t.value_with_sign
                transactions_count[ds] += 1
            else:
                limits_used[ds] = t.value_with_sign
                transactions_count[ds] = 1
        return (
            {
                ds: round(Decimal(limits_used), 2)
                for ds, limits_used in limits_used.items()
            },
            transactions_count,
        )

    @classmethod
    def setUpTestData(cls):
        """Setup."""
        cls.test_account = AccountFactory(tags__count=1, transactions__days=70)
        cls.test_user = cls.test_account.owner
        cls.limits_used, cls.transactions_count = cls._calculate_annotations(
            Transaction.objects.all()
        )

    def test_limit_used(self):
        """."""
        for ds, limit_used in self.limits_used.items():
            self.assertEqual(
                limit_used,
                Tag.objects.with_annotations(dateslot=ds)
                .values("limit_used")
                .first()["limit_used"],
            )

    def test_transactions_count(self):
        """."""
        for ds, transactions_count in self.transactions_count.items():
            self.assertEqual(
                transactions_count,
                Tag.objects.with_annotations(dateslot=ds)
                .values("transactions_count")
                .first()["transactions_count"],
            )
