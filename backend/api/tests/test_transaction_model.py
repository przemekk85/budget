from django.db.models.signals import post_save
from django.test import TestCase
from factory.django import mute_signals
import pendulum

from api.commons import TODAY
from api.enums import TransactionType
from api.factories import AccountFactory, TransactionFactory


class TestSystemBalanceIdProperty(TestCase):
    """Tests for 'Transaction' model - system_balance_id property.

    Transactions are generated from the first day three months back,
    for a period of 2 months and 7 days.

    """

    @classmethod
    def setUpTestData(cls):
        """Setup."""
        start_date = TODAY.replace(day=1).subtract(months=3)
        end_date = start_date.add(days=7, months=2)
        days = (end_date - start_date).days
        shift = (TODAY - end_date).days
        cls.test_account = AccountFactory(
            transactions__days=days, transactions__shift=shift
        )
        cls.test_user = cls.test_account.owner
        cls.transactions_queryset = (
            cls.test_account.transactions.with_system_balance_id()
        )
        latest_date = cls.test_account.transactions.values("created").last()["created"]
        cls.latest_date = pendulum.date(
            latest_date.year, latest_date.month, latest_date.day
        )

    def test_basic_genuine_data(self):
        """."""
        system_balance_transactions_count = self.transactions_queryset.filter(
            type=TransactionType.SYSTEM_BALANCE.value
        ).count()
        self.assertEqual(
            (system_balance_transactions_count - 1) * [True] + [False],
            [
                bool(t.system_balance_id)
                for t in self.transactions_queryset.filter(
                    type=TransactionType.SYSTEM_BALANCE.value
                )
            ],
        )
        self.assertEqual(None, self.transactions_queryset.last().system_balance_id)

    def test_balance_transaction_is_last(self):
        """."""
        with mute_signals(post_save):
            TransactionFactory(
                value=6666,
                type=TransactionType.BALANCE.value,
                account=self.test_account,
            )
        self.assertEqual(
            None,
            self.transactions_queryset.filter(
                created__gte=self.latest_date.replace(day=1)
            )
            .first()
            .system_balance_id,
        )
        self.assertEqual(
            None, self.transactions_queryset.last().system_balance_id,
        )

    def test_balance_transaction_is_in_the_middle(self):
        """."""
        test_transaction = self.transactions_queryset.last()
        following_month = self.latest_date.add(months=1).replace(day=1)
        with mute_signals(post_save):
            TransactionFactory(
                value=6666,
                type=TransactionType.BALANCE.value,
                account=self.test_account,
            )
            for i in range(3):
                TransactionFactory.create_batch(
                    3, created=self.latest_date.add(days=i), account=self.test_account,
                )
            TransactionFactory(
                value=9999,
                created=following_month,
                type=TransactionType.SYSTEM_BALANCE.value,
                account=self.test_account,
            )
        self.assertEqual(None, test_transaction.system_balance_id)
        self.assertEqual(
            True,
            bool(
                self.transactions_queryset.exclude(
                    type=TransactionType.SYSTEM_BALANCE.value
                )
                .last()
                .system_balance_id
            ),
        )
