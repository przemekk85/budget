from random import choice

from django.db.models import Count, Q
from django.urls import reverse

from api.enums import TransactionType
from api.models import Transaction
from api.tests.test_base import TestBaseDefault


class TestAsAccountAuthorized(TestBaseDefault):
    """Tests for 'transactions' endpoint - authorized."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].authorized.first()
        self.client.force_authenticate(user=self.test_user)

    def test_create_with_tags(self):
        """."""
        payload = {
            "value": 100,
            "type": TransactionType.CHARGE.value,
            "account": self.test_account.id,
            "tags": [choice(self.test_account.tags.values_list("id", flat=True))],
        }
        response = self.client.post(reverse("transaction-list"), payload)
        self.assertEquals(201, response.status_code)
        id = response.json().get("id")
        response = self.client.get(reverse("transaction-detail", args=[id]))
        self.assertEquals(200, response.status_code)

    def test_create_without_tags(self):
        """."""
        payload = {
            "value": 100,
            "type": TransactionType.CHARGE.value,
            "account": self.test_account.id,
        }
        response = self.client.post(reverse("transaction-list"), payload)
        self.assertEquals(201, response.status_code)
        id = response.json().get("id")
        response = self.client.get(reverse("transaction-detail", args=[id]))
        self.assertEquals(200, response.status_code)

    def test_delete_other_users(self):
        """."""
        test_transaction_id = (
            self.test_account.transactions.exclude(owner=self.test_user).first().id
        )
        response = self.client.delete(
            reverse("transaction-detail", args=[test_transaction_id])
        )
        self.assertEquals(403, response.status_code)

    def test_delete_own(self):
        """."""
        test_transaction_id = (
            self.test_account.transactions.filter(owner=self.test_user).first().id
        )
        response = self.client.delete(
            reverse("transaction-detail", args=[test_transaction_id])
        )
        self.assertEquals(204, response.status_code)

    def test_list_for_all_accounts(self):
        """."""
        response = self.client.get(reverse("transaction-list"))
        self.assertEquals(200, response.status_code)

    def test_list_for_single_account(self):
        """."""
        response = self.client.get(
            reverse("transaction-list"), {"aid": self.test_account.id}
        )
        self.assertEquals(200, response.status_code)

    def test_patch_other_users(self):
        """."""
        test_transaction = self.test_account.transactions.exclude(
            owner=self.test_user
        ).first()
        payload = {
            "value": 2 * test_transaction.value,
            "type": TransactionType.CHARGE.value
            if test_transaction.type == TransactionType.CREDIT.value
            else TransactionType.CREDIT.value,
        }
        response = self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.assertEquals(403, response.status_code)

    def test_patch_own(self):
        """."""
        test_transaction = self.test_account.transactions.filter(
            owner=self.test_user
        ).first()
        payload = {
            "value": float(2 * test_transaction.value),
            "type": TransactionType.CHARGE.value
            if test_transaction.type == TransactionType.CREDIT.value
            else TransactionType.CREDIT.value,
        }
        response = self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.assertEquals(200, response.status_code)
        response = self.client.get(
            reverse("transaction-detail", args=[test_transaction.id])
        )
        self.assertEquals(payload, {k: response.json().get(k) for k in payload.keys()})

    def test_retrieve_other_users(self):
        """."""
        test_transaction_id = (
            self.test_account.transactions.exclude(owner=self.test_user).first().id
        )
        response = self.client.get(
            reverse("transaction-detail", args=[test_transaction_id])
        )
        self.assertEquals(200, response.status_code)

    def test_retrieve_own(self):
        """."""
        test_transaction_id = (
            self.test_account.transactions.filter(owner=self.test_user).first().id
        )
        response = self.client.get(
            reverse("transaction-detail", args=[test_transaction_id])
        )
        self.assertEquals(200, response.status_code)


class TestAsAccountOwner(TestBaseDefault):
    """Tests for 'transactions' endpoint - owner."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].owner
        self.client.force_authenticate(user=self.test_user)

    def test_create_with_tags(self):
        """."""
        payload = {
            "value": 100,
            "type": TransactionType.CHARGE.value,
            "account": self.test_account.id,
            "tags": [choice(self.test_account.tags.values_list("id", flat=True))],
        }
        response = self.client.post(reverse("transaction-list"), payload)
        self.assertEquals(201, response.status_code)
        id = response.json().get("id")
        response = self.client.get(reverse("transaction-detail", args=[id]))
        self.assertEquals(200, response.status_code)

    def test_create_without_tags(self):
        """."""
        payload = {
            "value": 100,
            "type": TransactionType.CHARGE.value,
            "account": self.test_account.id,
        }
        response = self.client.post(reverse("transaction-list"), payload)
        self.assertEquals(201, response.status_code)
        id = response.json().get("id")
        response = self.client.get(reverse("transaction-detail", args=[id]))
        self.assertEquals(200, response.status_code)

    def test_delete_other_users(self):
        """."""
        test_transaction_id = (
            self.test_account.transactions.exclude(owner=self.test_user).first().id
        )
        response = self.client.delete(
            reverse("transaction-detail", args=[test_transaction_id])
        )
        self.assertEquals(204, response.status_code)

    def test_delete_own(self):
        """."""
        test_transaction_id = (
            self.test_account.transactions.filter(owner=self.test_user).first().id
        )
        response = self.client.delete(
            reverse("transaction-detail", args=[test_transaction_id])
        )
        self.assertEquals(204, response.status_code)

    def test_list_for_all_accounts(self):
        """."""
        response = self.client.get(reverse("transaction-list"))
        self.assertEquals(200, response.status_code)
        transactions_count = self.test_user.own_accounts.aggregate(
            transactions_count=Count(
                "transactions",
                filter=~Q(transactions__type=TransactionType.SYSTEM_BALANCE.value),
            )
        )["transactions_count"]
        self.assertEquals(transactions_count, len(response.json()))

    def test_list_for_single_account(self):
        """."""
        response = self.client.get(
            reverse("transaction-list"), {"aid": self.test_account.id}
        )
        self.assertEquals(200, response.status_code)
        self.assertEqual(
            self.test_account.transactions.exclude(
                type=TransactionType.SYSTEM_BALANCE.value
            ).count(),
            len(response.json()),
        )

    def test_patch_other_users(self):
        """."""
        test_transaction = self.test_account.transactions.exclude(
            owner=self.test_user
        ).first()
        payload = {
            "value": float(2 * test_transaction.value),
            "type": TransactionType.CHARGE.value
            if test_transaction.type == TransactionType.CREDIT.value
            else TransactionType.CREDIT.value,
        }
        response = self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.assertEquals(200, response.status_code)
        response = self.client.get(
            reverse("transaction-detail", args=[test_transaction.id])
        )
        self.assertEquals(payload, {k: response.json().get(k) for k in payload.keys()})

    def test_patch_own(self):
        """."""
        test_transaction = self.test_account.transactions.filter(
            owner=self.test_user
        ).first()
        payload = {
            "value": float(2 * test_transaction.value),
            "type": TransactionType.CHARGE.value
            if test_transaction.type == TransactionType.CREDIT.value
            else TransactionType.CREDIT.value,
        }
        response = self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.assertEquals(200, response.status_code)
        response = self.client.get(
            reverse("transaction-detail", args=[test_transaction.id])
        )
        self.assertEquals(payload, {k: response.json().get(k) for k in payload.keys()})

    def test_retrieve_other_users(self):
        """."""
        test_transaction_id = (
            self.test_account.transactions.exclude(owner=self.test_user).first().id
        )
        response = self.client.get(
            reverse("transaction-detail", args=[test_transaction_id])
        )
        self.assertEquals(200, response.status_code)

    def test_retrieve_own(self):
        """."""
        test_transaction_id = (
            self.test_account.transactions.filter(owner=self.test_user).first().id
        )
        response = self.client.get(
            reverse("transaction-detail", args=[test_transaction_id])
        )
        self.assertEquals(200, response.status_code)


class TestAsUnauthorized(TestBaseDefault):
    """Tests for 'transactions' endpoint - unauthorized."""

    def setUp(self):
        """"Setup."""
        self.test_account = self.accounts[1]
        self.test_user = self.accounts[0].authorized.first()
        self.client.force_authenticate(user=self.test_user)

    def test_delete_other_users(self):
        """."""
        test_transaction_id = self.test_account.transactions.first().id
        response = self.client.delete(
            reverse("transaction-detail", args=[test_transaction_id])
        )
        self.assertEquals(403, response.status_code)

    def test_list_for_single_account(self):
        """."""
        response = self.client.get(
            reverse("transaction-list"), {"aid": self.test_account.id}
        )
        self.assertEquals(403, response.status_code)

    def test_patch_other_users(self):
        """."""
        test_transaction = self.test_account.transactions.first()
        payload = {
            "value": 2 * test_transaction.value,
            "type": TransactionType.CHARGE.value
            if test_transaction.type == TransactionType.CREDIT.value
            else TransactionType.CREDIT.value,
        }
        response = self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.assertEquals(403, response.status_code)

    def test_retrieve_other_users(self):
        """."""
        test_transaction_id = self.test_account.transactions.first().id
        response = self.client.get(
            reverse("transaction-detail", args=[test_transaction_id])
        )
        self.assertEquals(403, response.status_code)


class TestFaultyRequest(TestBaseDefault):
    """Tests for 'transactions' endpoint - faulty request."""

    def setUp(self):
        """"Setup."""
        self.test_user = self.accounts[0].owner
        self.client.force_authenticate(user=self.test_user)

    def test_create_with_incomplete_payload(self):
        """."""
        test_tags = choice(self.accounts[0].tags.values_list("id", flat=True))
        payload = {
            "value": 100,
            "account": self.accounts[0].id,
            "tags": test_tags,
        }
        response = self.client.post(reverse("transaction-list"), payload)
        self.assertEquals(400, response.status_code)

    def test_create_with_tags_applicable_validator(self):
        """."""
        test_tags = choice(self.accounts[0].tags.values_list("id", flat=True))
        payload = {
            "value": 100,
            "type": TransactionType.BALANCE.value,
            "account": self.accounts[0].id,
            "tags": test_tags,
        }
        response = self.client.post(reverse("transaction-list"), payload)
        self.assertEquals(400, response.status_code)

    def test_create_with_tags_in_tags_validator(self):
        """."""
        test_tags = choice(self.accounts[0].tags.values_list("id", flat=True))
        payload = {
            "value": 100,
            "type": TransactionType.CHARGE.value,
            "account": self.accounts[1].id,
            "tags": test_tags,
        }
        response = self.client.post(reverse("transaction-list"), payload)
        self.assertEquals(400, response.status_code)

    def test_create_with_unallowed_type(self):
        """."""
        payload = {
            "value": 100,
            "type": TransactionType.SYSTEM_BALANCE.value,
            "account": self.accounts[0].id,
        }
        response = self.client.post(reverse("transaction-list"), payload)
        self.assertEquals(400, response.status_code)

    def test_patch_with_tags_applicable_validator_1(self):
        """."""
        test_transaction = choice(
            self.accounts[0].transactions.exclude(
                type=TransactionType.SYSTEM_BALANCE.value
            )
        )
        payload = {
            "value": 2 * test_transaction.value,
            "type": TransactionType.BALANCE.value,
        }
        response = self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.assertEquals(400, response.status_code)

    def test_patch_with_tags_applicable_validator_2(self):
        """."""
        test_transaction = choice(
            self.accounts[0].transactions.exclude(
                type=TransactionType.SYSTEM_BALANCE.value
            )
        )
        # change type for the test purposes
        test_transaction.type = TransactionType.BALANCE.value
        test_transaction.save()
        test_tags = [choice(self.accounts[0].tags.all()).id]
        payload = {
            "value": 2 * test_transaction.value,
            "tags": test_tags,
        }
        response = self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.assertEquals(400, response.status_code)

    def test_patch_with_tags_in_tags_validator(self):
        """."""
        test_transaction = choice(
            self.accounts[0].transactions.exclude(
                type=TransactionType.SYSTEM_BALANCE.value
            )
        )
        test_type = (
            TransactionType.CHARGE.value
            if test_transaction.type == TransactionType.CREDIT.value
            else TransactionType.CREDIT.value
        )
        test_tags = [choice(self.accounts[1].tags.all()).id]
        payload = {
            "value": 2 * test_transaction.value,
            "type": test_type,
            "tags": test_tags,
        }
        response = self.client.patch(
            reverse("transaction-detail", args=[test_transaction.id]), payload
        )
        self.assertEquals(400, response.status_code)


class TestFilterSet(TestBaseDefault):
    """Tests for 'transactions' endpoint - filters."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].owner
        self.client.force_authenticate(user=self.test_user)

    def test_dateslot_filter(self):
        """."""
        dateslots = [
            (
                year,
                month,
                Transaction.objects.exclude(type=TransactionType.SYSTEM_BALANCE.value)
                .filter(created__year=year, created__month=month)
                .count(),
            )
            for year, month in {
                (t.created.year, t.created.month)
                for t in self.test_account.transactions.all()
            }
        ]
        for year, month, count in dateslots:
            response = self.client.get(
                reverse("transaction-list"), {"month": month, "year": year}
            )
            self.assertEquals(count, len(response.json()))
