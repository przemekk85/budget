from django.urls import reverse

from api.models import Transaction
from api.tests.helpers import get_dateslots
from api.tests.test_base import TestBaseLong


class TestAsAccountAuthorized(TestBaseLong):
    """Tests for 'dateslots' endpoint - authorized."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].authorized.first()
        self.client.force_authenticate(user=self.test_user)

    def test_list_for_all_accounts(self):
        """."""
        response = self.client.get(reverse("dateslots-full-list"))
        self.assertEquals(200, response.status_code)
        self.assertEquals(
            get_dateslots(qs=self.test_account.transactions.all()),
            get_dateslots(response=response),
        )

    def test_list_for_single_account(self):
        """."""
        response = self.client.get(
            reverse("dateslots-account-list", args=[self.test_account.id])
        )
        self.assertEquals(200, response.status_code)
        self.assertEquals(
            get_dateslots(qs=self.test_account.transactions.all()),
            get_dateslots(response=response),
        )


class TestAsAccountOwner(TestBaseLong):
    """Tests for 'dateslots' endpoint - owner."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].owner
        self.client.force_authenticate(user=self.test_user)

    def test_list_for_all_accounts(self):
        """."""
        response = self.client.get(reverse("dateslots-full-list"))
        self.assertEquals(200, response.status_code)
        self.assertEquals(
            get_dateslots(qs=Transaction.objects.all()),
            get_dateslots(response=response),
        )

    def test_list_for_single_account(self):
        """."""
        response = self.client.get(
            reverse("dateslots-account-list", args=[self.test_account.id])
        )
        self.assertEquals(200, response.status_code)
        self.assertEquals(
            get_dateslots(qs=self.test_account.transactions.all()),
            get_dateslots(response=response),
        )


class TestAsUnauthorized(TestBaseLong):
    """Tests for 'dateslots' endpoint - unauthorized."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[1]
        self.test_user = self.accounts[0].authorized.first()
        self.client.force_authenticate(user=self.test_user)

    def test_list_for_single_account(self):
        """."""
        response = self.client.get(
            reverse("dateslots-account-list", args=[self.test_account.id])
        )
        self.assertEquals(403, response.status_code)


class TestFaultyRequest(TestBaseLong):
    """Tests for 'transactions' endpoint - faulty input."""

    def setUp(self):
        """Setup."""
        self.test_account = self.accounts[0]
        self.test_user = self.accounts[0].authorized.first()
        self.client.force_authenticate(user=self.test_user)

    def test_list_for_non_existing_account(self):
        """."""
        response = self.client.get(reverse("dateslots-account-list", args=[999]))
        self.assertEquals(404, response.status_code)
